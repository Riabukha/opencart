package com.openCartPages;

import org.junit.Before;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    JavascriptExecutor js;

    By macBookPicture1 = By.cssSelector("li:nth-child(1) img");
    By macBookPicture2 = By.cssSelector(".image-additional:nth-child(2) img");
    By macBookPicture3 = By.cssSelector(".image-additional:nth-child(3) img");
    By macBookPicture4 = By.cssSelector(".image-additional:nth-child(4) img");
    By macBookPicture5 = By.cssSelector(".image-additional:nth-child(5) img");
    By addToWishListBtn = By.cssSelector(".btn > .fa-heart");
    By CompareBtn = By.cssSelector(".fa-exchange");
    By arrowRight = By.cssSelector(".mfp-arrow-right");
    By arrowLeft = By.cssSelector(".mfp-arrow-left");
    By crossButton = By.cssSelector(".mfp-close");
    By quantityField = By.id("input-quantity");
    By addToCartButton = By.id("button-cart");
    By specificationTab = By.linkText("Specification");
    By reviewsTab = By.linkText("Reviews (0)");
    By descriptionTab = By.linkText("Description");

    /*@Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
    }*/
}



