package com.openCartPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public WebDriver driver;
    public WebDriverWait wait;

    public BasePage(WebDriver driver) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\chromedriver_win32\\chromedriver.exe");
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
    }

    public void click(By elementBy) {
        driver.findElement(elementBy).click();
    }

    public void writeText(By elementBy, String text) {
        driver.findElement(elementBy).sendKeys(text);
    }
}


