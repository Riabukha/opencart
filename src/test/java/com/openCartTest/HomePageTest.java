package com.openCartTest;

import com.openCartPages.HomePage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class HomePageTest extends BaseTest {
    @Test
    public void macBookSliderClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToMacBookSlider();
        Assert.assertEquals("MacBook", driver.getTitle());
    }

    @Test
    public void iPhoneSliderClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToIphoneSlider();
        Assert.assertEquals("iPhone6", driver.getTitle());
    }

    @Test
    public void sliderMoving() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .moveSliderManually();
    }

    @Test
    public void homePageReturning() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .returnToHomePage();
        Assert.assertEquals("Your Store", driver.getTitle());
    }

    @Test
    public void macBookFeaturedClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToMacBookPageFromFeatured();
        Assert.assertEquals("MacBook", driver.getTitle());
    }

    @Test
    public void iPhoneFeaturedClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToIphonePageFromFeatured();
        Assert.assertEquals("iPhone", driver.getTitle());
    }

    @Test
    public void appleCinemaFeaturedClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToAppleCinemaPageFromFeatured();
        Assert.assertEquals("Apple Cinema 30", driver.getTitle());
    }

    @Test
    public void canonEosFeaturedClickability() {
        HomePage homePage = new HomePage(driver);
        homePage.goToURL()
                .goToCanonEosPageFromFeatured();
        Assert.assertEquals("Canon EOS 5D", driver.getTitle());
    }

}


