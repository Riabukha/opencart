package com.openCartPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    protected final String baseURL = "http://taqc-dp170.zzz.com.ua/index.php?route=common/home";
    By logoBtn = By.id("logo");
    By arrowRight = By.cssSelector("#slideshow0 .owl-next > .fa");
    By arrowLeft = By.cssSelector("#slideshow0 .owl-prev > .fa");
    By iPhoneSlider = By.xpath("//img[@alt='iPhone 6']");
    By macBookSlider = By.xpath("//img[@alt='MacBookAir']");
    By macBookFeatured = By.cssSelector(".product-layout:nth-child(1) .img-responsive");
    By iPhoneFeatured = By.cssSelector(".product-layout:nth-child(2) .img-responsive");
    By appleCinemaFeatured = By.cssSelector(".product-layout:nth-child(3) .img-responsive");
    By canonEosFeatured = By.cssSelector(".product-layout:nth-child(4) .img-responsive");


    public HomePage goToURL() {
        driver.get(baseURL);
        driver.manage().window().maximize();
        return this;
    }

    public ProductPage goToIphoneSlider() {
        click(iPhoneSlider);
        return new ProductPage(driver);
    }

    public ProductPage goToMacBookSlider() {
        click(macBookSlider);
        return new ProductPage(driver);
    }

    public HomePage moveSliderManually() {
        click(arrowLeft);
        click(arrowRight);
        return this;
    }

    public HomePage returnToHomePage() {
        click(iPhoneSlider);
        click(logoBtn);
        return this;
    }

    public ProductPage goToMacBookPageFromFeatured() {
        click(macBookFeatured);
        return new ProductPage(driver);
    }

    public ProductPage goToIphonePageFromFeatured() {
        click(iPhoneFeatured);
        return new ProductPage(driver);
    }

    public ProductPage goToAppleCinemaPageFromFeatured() {
        click(appleCinemaFeatured);
        return new ProductPage(driver);
    }

    public ProductPage goToCanonEosPageFromFeatured() {
        click(canonEosFeatured);
        return new ProductPage(driver);
    }
}
